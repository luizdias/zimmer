var express = require('express');
var app = express();

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});

// app.get('/', function (req, res) {
//   res.render('index', {});
// });

app.get('/', function (req, res) {
	res.send("Hello World!");
});

var cron = require('node-cron');
 
cron.schedule('* * * * *', function(){
	var strava = require('strava-v3');
	strava.athlete.get({},function(err,payload) {
	    if(!err) {
	    	// do something with the payload here
	        console.log(payload);
	    }
	    else {
	        console.log(err);
	    }
	});
  console.log('Checking STRAVA every minute');
});

var http = require('http');

var qs = require('querystring');

var options = {
  host: 'mobi.desenv.bb.com.br',
  port: 8081,
  path: '/mov-centralizador/servico/ServicoLogin/login/entrada' + '?' + qs.stringify({numeroContratoOrigem: '40100', dependenciaOrigem:'79847', senhaConta:'88888888', titularidade:'1'})
};

console.log(options);

http.get(options, function(resp){
	resp.setEncoding('utf8')
	resp.on('data', function(chunk){
		console.log(chunk);
	//do something with chunk
  });
}).on("error", function(e){
  console.log("Got error: " + e.message);
});